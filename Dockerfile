FROM python:3
ENV PYTHONUNBUFFERED 1
WORKDIR .
RUN apt-get update \
    && apt-get -y install libpq-dev gcc \
    && pip install psycopg2
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .